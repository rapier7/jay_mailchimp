﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic;

namespace MarkdownConverter.Controllers.Api
{
    [Route("api/markdown")]
    [ApiController]
    public class MarkdownConverterAPI : ControllerBase
    {
        [Route("convert")]
        [HttpPost]
        public IActionResult ConvertMarkdownToHtml([FromBody] Payload payload)
        {
            var converted = Converter.ToHtml(payload.input);
            return Ok(converted);
        }

        public class Payload
        {
            public string input { get; set; }
        }
    }
}
