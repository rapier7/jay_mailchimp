﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$("#convert").click(() => {
    var str = $("#markdown").val();

    var request = $.ajax({
        url: "api/markdown/convert",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({ input: str }),
    });

    request.done((result) => {
        $("#output").text(result);
        $("#outputHtml").html(result);
    });
});