using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logic;
using System.Linq;
using System.Text;

namespace MarkdownTest
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void SanityCheck()
        {
            var text = @"This is sample markdown for the [Mailchimp](https://www.mailchimp.com) homework assignment.";

            var converted = Converter.ToHtml(text);

            Assert.IsTrue(converted.Contains("<p>"));
            Assert.IsTrue(converted.Contains("</p>"));
            Assert.IsTrue(converted.Contains("<a href='https://www.mailchimp.com'>Mailchimp</a>"));
        }

        [TestMethod]
        public void TestHeader()
        {
            var h1 = "# header";
            var h2 = "## header";
            var h3 = "### header";
            var h4 = "#### header";
            var h5 = "##### header";
            var h6 = "###### header";
            var h7 = "####### header";

            var c1 = Converter.Header(h1);
            var c2 = Converter.Header(h2);
            var c3 = Converter.Header(h3);
            var c4 = Converter.Header(h4);
            var c5 = Converter.Header(h5);
            var c6 = Converter.Header(h6);
            var c7 = Converter.Header(h7);

            Assert.IsTrue(c1.Equals("<h1>header</h1>\n"));
            Assert.IsTrue(c2.Equals("<h2>header</h2>\n"));
            Assert.IsTrue(c3.Equals("<h3>header</h3>\n"));
            Assert.IsTrue(c4.Equals("<h4>header</h4>\n"));
            Assert.IsTrue(c5.Equals("<h5>header</h5>\n"));
            Assert.IsTrue(c6.Equals("<h6>header</h6>\n"));
            Assert.IsTrue(c7.Equals("<h6> header</h6>\n"));
        }

        [TestMethod]
        public void TestLinks()
        {
            var linkString = "This is a paragraph [with an inline link](http://google.com). Neat, eh? Here's another [link](http://yahoo.com).";
            var linkOutput = Converter.HandleLinks(linkString);

            Assert.IsTrue(linkOutput.Contains("<a href='http://google.com'>with an inline link</a>"));
            Assert.IsTrue(linkOutput.Contains("<a href='http://yahoo.com'>link</a>"));
        }
    }
}
