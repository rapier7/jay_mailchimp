﻿using System;
using System.Text;
using System.Linq;

namespace Logic
{
    public static class Converter
    {
        public static string ToHtml(string input)
        {
            var returnable = new StringBuilder();

            var precededByParagraph = false;

            var lines = input.Split("\n");

            foreach(var line in lines)
            {
                var formatted=HandleLine(line, ref precededByParagraph);
                returnable.Append(formatted);
            }

            //One of my tests caught this!
            if (precededByParagraph)
            {
                returnable.Append("</p>");
            }

            return returnable.ToString();
        }

        public static string HandleLine(string line, ref bool precededByParagraph)
        {
            var sb = new StringBuilder();
            var head = Header(line);

            if (string.IsNullOrEmpty(line)) //If empty line
            {
                if (precededByParagraph)
                {
                    sb.Append("</p>\n\n");
                }
                else
                {
                    sb.Append("\n");
                }

                precededByParagraph = false;
            }
            else if (!head.Equals(line)) //If header
            {
                if (precededByParagraph)
                {
                    head = "</p>\n" + head;
                }
                sb.Append(HandleLinks(head));

                precededByParagraph = false;
            }
            else //Line is <p> or part of one
            {
                var append = HandleLinks(line);
                if (precededByParagraph)
                {
                    sb.Append("\n" + append);
                }
                else
                {
                    sb.Append("<p>" + append);
                }

                precededByParagraph = true;
            }

            return sb.ToString();
        }

        public static string Header(string line)
        {
            var isHeader = false;
            var headerSize = 0;

            while (line.IndexOf("#") == 0 && headerSize < 6)
            {
                isHeader = true;
                headerSize++;
                line = line.Remove(0, 1);
            }

            if (isHeader)
            {
                line = line.Remove(0, 1); //remove whitespace after the #s
                line = "<h" + headerSize + ">" + line + "</h" + headerSize + ">\n";
            }
            return line;
        }

        public static string HandleLinks(string line)
        {
            while (line.IndexOf("[") != -1)
            {
                var linkNameStart = line.IndexOf("[") + 1;
                var linkNameEnd = line.IndexOf("]");
                var linkName = line.Substring(linkNameStart, linkNameEnd - linkNameStart);

                var urlStart = line.IndexOf("(") + 1;
                var urlEnd = line.IndexOf(")");
                var url = line.Substring(urlStart, urlEnd - urlStart);

                line = line.Substring(0, linkNameStart - 1) + "<a href='" + url + "'>" + linkName + "</a>" + line.Substring(urlEnd + 1);
            }
            return line;
        }
    }
}
